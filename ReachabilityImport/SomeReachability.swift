//
//  SomeReachability.swift
//  ReachabilityImport
//
//  Created by Jaim Zuber on 7/28/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import Foundation


class SomeReachability {
    
    func doSomething() {
        
        Reachability.reachabilityForInternetConnection()
    }
    
}