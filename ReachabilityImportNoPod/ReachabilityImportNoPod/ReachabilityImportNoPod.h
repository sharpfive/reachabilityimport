//
//  ReachabilityImportNoPod.h
//  ReachabilityImportNoPod
//
//  Created by Jaim Zuber on 7/28/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ReachabilityImportNoPod.
FOUNDATION_EXPORT double ReachabilityImportNoPodVersionNumber;

//! Project version string for ReachabilityImportNoPod.
FOUNDATION_EXPORT const unsigned char ReachabilityImportNoPodVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ReachabilityImportNoPod/PublicHeader.h>

#import <Reachability.h>


